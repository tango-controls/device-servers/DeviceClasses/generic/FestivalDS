#!/usr/bin/env python

# FestivalDS - Text to speech tango device server using festival

# Copyright 2013-2019 by CELLS / ALBA Synchrotron, Cerdanyola del Valles, Spain

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import os

import time
import threading

try:
    import tango
    DEVICE_IMPL = tango.LatestDeviceImpl
except Exception as e:
    print("Import ing PyTango as tango: {}".format(e))
    import PyTango as tango
    DEVICE_IMPL = tango.Device_4Impl


#==================================================================
#   FestivalDS Class Description:
#
#
#=================================================================

def trace(msg):
  print("{}: {}".format(time.strftime('%Y-%m-%d %H:%M:%S'), msg))

def os_cmd(cmd,term=True):
    trace( "os:> {}".format(cmd))
    if term and not cmd.strip().endswith('&'): cmd+=' &'
    os.system(cmd)

def Sequence(repeat, beep_cmd, festival_cmd):
    trace( 'In Sequence Thread ...')
    ev = threading.Event()
    for i in range(repeat):
        os_cmd(beep_cmd, term=False)
        ev.wait(0.5)
    os_cmd(festival_cmd, term=False)

class FestivalDS(DEVICE_IMPL):

#--------- Add you global variables here --------------------------

#------------------------------------------------------------------
#	Device constructor
#------------------------------------------------------------------
    def __init__(self,cl, name):
        DEVICE_IMPL.__init__(self,cl,name)
        FestivalDS.init_device(self)

#------------------------------------------------------------------
#	Device destructor
#------------------------------------------------------------------
    def delete_device(self):
        trace("[Device delete_device method] for device{}".format(self.get_name()))
        if self.thread and self.thread.is_alive(): self.thread.join(10.)


#------------------------------------------------------------------
#	Device initialization
#------------------------------------------------------------------
    def init_device(self):
        trace("In {}::init_device()".format(self.get_name()))
        self.thread = None
        self.set_state(tango.DevState.ON)
        self.get_device_properties(self.get_device_class())
        

#------------------------------------------------------------------
#	Always excuted hook method
#------------------------------------------------------------------
    def always_executed_hook(self):
        trace("In {}::always_excuted_hook()".format(self.get_name()))

#==================================================================
#
#	FestivalDS read/write attribute methods
#
#==================================================================
#------------------------------------------------------------------
#	Read Attribute Hardware
#------------------------------------------------------------------
    def read_attr_hardware(self,data):
        trace("In {}::read_attr_hardware()".format(self.get_name()))

    ### Commands ###
    def Play(self,text):
        trace("In {}.Play({})".format(self.get_name(),text))
        out = self.Cmd_Festival.format(text)
        os_cmd(out)

    def Beep(self):
        trace("In {}.Beep()".format(self.get_name()))
        if not os.path.isfile(self.Beep_Path):
            raise Exception("Wav file not set")
        os_cmd(self.Cmd_Play.format(self.Beep_Path))

    def PopUp(self,argin):
        if len(argin) == 1:
            title, text, period = argin[0], '', 0
        else:
            title = argin[0]
            text = argin[1] if len(argin)>1 else ''
            period = int(argin[2]) if len(argin)>2 else 0
        command = self.Cmd_PopUp.format(self.Display, title, 1000*(period or 60))
        if (self.Icon is not None) and (os.path.isfile(self.Icon)):
            command += " -i {}".format(self.Icon)
        if text: 
            command +=' "{}"'.format(text.replace('<br/>','\n').replace('<p>','\n').replace('</p>','\n').replace('<br>','\n'))
        os_cmd(command)
        return title #command
     
    def Play_Sequence(self, text):
        trace("In {}.Play_Sequence({})".format(self.get_name(), text))
        beep = self.Cmd_Play.format(self.Beep_Path)
        speech = self.Cmd_Festival.format(text)
        self.thread = threading.Thread(
            target=Sequence, args=(self.Default_Repeat_Times, beep, speech))
        self.thread.start()        


#==================================================================
#
#	FestivalDS command methods
#
#==================================================================

#==================================================================
#
#	FestivalDSClass class definition
#
#==================================================================
class FestivalDSClass(tango.DeviceClass):

	#	Class Properties
    class_property_list = {
        }

    #	Device Properties
    device_property_list = {
        'Beep_Path':
            [tango.DevString,
            "",
            None ],
        'Default_Repeat_Times':
            [tango.DevShort,
            "",
            1 ],
        'Display':
            [tango.DevString,
            "Display to show popup mesages",
            [':0'] ],
        'Icon':
            [tango.DevString,
            "Icon for notifications",
            None ],
        'Cmd_Festival':
            [tango.DevString,
            "System command to play festival speech",
            "echo \"{}\" | padsp festival --tts" ],
        'Cmd_Play':
            [tango.DevString,
            "System command to play a sound",
            "padsp aplay {}" ],
        'Cmd_PopUp':
            [tango.DevString,
            "System command to show pop ups",
            "DISPLAY={} sudo -u operator notify-send \"{}\" -u critical -t {}" ],
        }

	#	Command definitions
    cmd_list = {    
    'Play' : 
            [[tango.DevString, "Play a given Text; requires Festival library"], 
            [tango.DevVoid, ""]], 
    'Beep' : 
            [[tango.DevVoid, "Force a Beep Sound; requires Festival library "], 
            [tango.DevVoid, ""]], 
    'Play_Sequence' : 
            [[tango.DevString, "Force a Beep Sound with a given speech;  requires Festival library"], 
            [tango.DevVoid, ""]], 
    'PopUp' : 
            [[tango.DevVarStringArray, "PopUp(title,text,[seconds]); Shows a system pop-up; requires libnotify-tools package"], 
            [tango.DevString, ""]],             
        }


	#	Attribute definitions
    attr_list = {
        }


#------------------------------------------------------------------
#	FestivalDSClass Constructor
#------------------------------------------------------------------
    def __init__(self, name):
        tango.DeviceClass.__init__(self, name)
        self.set_type(name)
        print("In FestivalDSClass  constructor")

#==================================================================
#
#	FestivalDS class main method
#
#==================================================================
def main():
    try:
        py = tango.Util(sys.argv)
        py.add_TgClass(FestivalDSClass,FestivalDS,'FestivalDS')

        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print("-------> Received a DevFailed exception: {}".format(e))
    except Exception as e:
        print("-------> An unforeseen exception occured.... {}".format(e))


if __name__ == '__main__':
    main()
